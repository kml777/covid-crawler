from django.shortcuts import redirect
from django.http import HttpResponse
from . import crawlers
from django.views import generic
from django.template import loader

# Create your views here.

def cnn(request):
    crawler = crawlers.CnnCrawler()
    article = request.GET.get('article', None)
    if article is None:
        titles = crawler.get_titles()
        template = loader.get_template('crawler/cnn.html')
        context = {'titles' : titles}
        return HttpResponse(template.render(context, request))
    else:
        body = crawler.get_body(title=article[:-1])
        template = loader.get_template('crawler/cnn_body.html')
        context = {'paragraphs':body}
        return HttpResponse(template.render(context, request))

def who(request):
    crawler = crawlers.WhoCrawler()
    article = request.GET.get('article', None)
    if article is None:
        titles = crawler.get_titles()
        template = loader.get_template('crawler/who.html')
        context = {'titles' : titles}
        return HttpResponse(template.render(context, request))
    else:
        url = crawler.get_url(title=article[:-1])
        print("!!!!")
        print(url)
        print("?????")
        return redirect(url)

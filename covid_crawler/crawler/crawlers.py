import configs
import requests
from bs4 import BeautifulSoup


class Content:
    def __init__(self, content, base_url, url_extractor=lambda content: content.get('href')):
        self.content = content
        self._base_url = base_url
        self.url = None
        self.title = None
        self.body = None
        self.url_extractor = url_extractor

    def get_url(self):
        if self.url is None:
            print("!!!")
            print(self._base_url)
            print("???")
            print(self.url_extractor(self.content))
            print("***")
            self.url = self.url_extractor(
                self.content) if self._base_url is None else self._base_url[:-1] + self.url_extractor(self.content)

        return self.url

    def get_title(self):
        if self.title is None:
            self.title = self.content.get_text()
        return self.title

    def get_body(self):
        if self.body is None:
            request = requests.get(self.get_url())
            web_page = BeautifulSoup(request.content, 'lxml')
            self.body = [paragraph.get_text() for paragraph in web_page.find_all('p')][:-3]
        return self.body


class Crawler:
    def __init__(self, base_url):
        self.request = requests.get(base_url)
        self.web_page = BeautifulSoup(self.request.content, 'lxml')
        self.results = None

    def _contents_check(self):
        if self.results is None:
            self._collect_contents()

    def get_titles(self):
        self._contents_check()
        titles = [result.get_title() for result in self.results][:configs.NUMBER_OF_CONTENTS]
        return titles


class CnnCrawler(Crawler):
    def __init__(self):
        Crawler.__init__(self=self, base_url=configs.CNN_URL)

    def _collect_contents(self):
        self.results = []
        contents = self.web_page.find('ul')
        for article in contents.find_all('a'):
            content = Content(content=article, base_url=configs.CNN_URL)
            if 'covid' in content.get_title().lower() or 'corona' in content.get_title().lower():
                self.results.append(content)

    def get_body(self, title):
        self._contents_check()
        for article in self.results:
            if article.get_title() == title:
                return article.get_body()


class WhoCrawler(Crawler):
    def __init__(self):
        Crawler.__init__(self=self, base_url=configs.WHO_URL)

    def _collect_contents(self):
        self.results = [Content(content=result, base_url="", url_extractor=lambda content: content.find('a').get('href'))
                        for result in
                        self.web_page.find_all('div', class_='titleArt')]

    def get_url(self, title):
        self._contents_check()
        for content in self.results:
            if content.get_title().strip() == title.strip():
                return content.get_url()
